# --------------------------------------------------------------------------
# ---------------------------  Cryptography  -------------------------------
# --------------------------------------------------------------------------
#  Main of the Glibc's generator to crack
#  Author: J. Wrzodek
#  2020
# --------------------------------------------------------------------------
# --------------------------------------------------------------------------
import glibc_gen
import glibc_crack


def form_test_file():
    with open('wrong_data.txt', 'w') as data:
        for value in range(1, 1000):
            print(((12345 - value)**4 & 0xffffffff) >> 1, file=data)


def main():
    seed_to_test = 1234

    print('__________CORRECT NUMBERS TO CHECK__________\n')
    glibc_gen.form_file(seed_to_test, 1000)
    glibc_crack.run()
    print('\n__________WRONG NUMBERS TO CHECK____________\n')
    form_test_file()
    glibc_crack.run('wrong_data.txt')


if __name__ == "__main__":
    main()