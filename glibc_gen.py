# --------------------------------------------------------------------------
# ---------------------------  Cryptography  -------------------------------
# --------------------------------------------------------------------------
#  The GLIBC pseudo-random number generator
#  Author: J. Wrzodek
#  2020
# --------------------------------------------------------------------------
# --------------------------------------------------------------------------


def run(seed, n):
    """
        :param seed: enter seed
        :param n: size of generated string of pseudo-random numbers
        :return: string of pseudo-random numbers generated from seed
    """
    gen_vls = []
    res_vls = []
    gen_vls.append(seed)
    for i in range(1, 31):
        gen_vls.append((16807 * gen_vls[i - 1]) % 2147483647)
        if gen_vls[i] < 0:
            gen_vls[i] += 2147483647
    for i in range(31, 34):
        gen_vls.append(gen_vls[i - 31])
    for i in range(34, 344):
        gen_vls.append(gen_vls[i - 31] + gen_vls[i - 3])
    for i in range(344, n + 344):
        gen_vls.append(gen_vls[i - 31] + gen_vls[i - 3])
        res_vls.append((gen_vls[i] & 0xffffffff) >> 1)
    return res_vls


def form_file(seed, n, name='data.txt'):
    """
        :param seed: enter seed
        :param n: size of generated string of pseudo-random numbers
        :param name: name within extension of file filled by pseudo-random numbers to form
    """
    ran_vls = run(seed, n)
    with open(name, 'w') as data:
        for value in ran_vls:
            print(value, file=data)
